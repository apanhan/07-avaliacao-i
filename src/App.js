import React, { Component } from "react";
import "./App.css";
import data from "./products.json";

// Use o método getProducts para buscar os produtos simulando uma requisição e substituindo a importação do arquivo products.json.
import { getProducts } from "./utils/product";

class App extends Component {
  state = {
    products: data.products,
  };

  render() {
    const productExample = this.state.products[0];

    return (
      <div className="wishlist">
        <h1>Wishlist</h1>
        <section>
          <h2>Sua lista de desejos</h2>
          <div className="product-list">
            <div className="product-item">
              <img
                src={productExample.image}
                alt={productExample.name}
                className="product-image"
                width="200"
              />
              <h3 className="product-name">{productExample.name}</h3>
              <div className="product-price">
                <p className="price-from">
                  de{" "}
                  {productExample.priceFrom.toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })}
                </p>
                <p className="price">
                  por{" "}
                  {productExample.price.toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })}
                </p>
              </div>
              <button className="add-to-wishlist">
                - Remover da Wishlist
              </button>
            </div>
          </div>
        </section>
        <section>
          <h2>Adicione à lista de desejos</h2>
          <div className="product-list">
            <div className="product-item">
              <img
                src={productExample.image}
                alt={productExample.name}
                className="product-image"
                width="200"
              />
              <h3 className="product-name">{productExample.name}</h3>
              <div className="product-price">
                <p className="price-from">
                  de{" "}
                  {productExample.priceFrom.toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })}
                </p>
                <p className="price">
                  por{" "}
                  {productExample.price.toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })}
                </p>
              </div>
              <button className="add-to-wishlist">
                ♡ Adicionar à Wishlist
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;
